from 9cbcf5986c55

RUN apt-get update && apt-get install -y \
    libjson-perl lzop git ncurses-dev

VOLUME  /user
WORKDIR /user
USER    user
ENV     USER=user
